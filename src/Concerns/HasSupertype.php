<?php

namespace Empu\EloquentSubtype\Concerns;

use Empu\EloquentSubtype\Contracts\InheritableEntity;
use Empu\EloquentSubtype\Scopes\ConcatSupertype;
use ErrorException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Arr;

trait HasSupertype
{
    /**
     * The super-type key for the model.
     *
     * @var string
     */
    protected $supertypeKey = 'super_id';

    /**
     * The super-type model's attributes.
     *
     * @var array
     */
    protected $supertypeAttributes = [];

    /**
     * The name of supertype relation
     * @var string
     */
    // protected $supertype = '<belongs-to-relation>';

    public static function bootHasSupertype(): void
    {
        static::saving(function (Model $model) {
            $model->separateSupertypeAttributes();
            $model->commitSupertype();
        });

        static::addGlobalScope(new ConcatSupertype());
    }

    public function initializeHasSupertype()
    {
        if (!property_exists($this, 'supertype')) {
            $className = get_class($this);
            throw new ErrorException("Undefined property 'supertype' in class {$className}.");
        }
    }

    public function supertype(): InheritableEntity
    {
        return $this->exists
            ? $this->supertypeRelation()->firstOrFail()
            : $this->supertypeModel();
    }

    /**
     * Helper
    */
    public function supertypeRelation(): Relation
    {
        return $this->{$this->supertype}();
    }

    public function supertypeModel(): Model
    {
        return $this->supertypeRelation()->getRelated();
    }

    public function separateSupertypeAttributes(): void
    {
        $supertypeColumns = $this->supertype()->descendibleColumns();
        $this->supertypeAttributes = Arr::only($this->getAttributes(), $supertypeColumns);

        foreach ($supertypeColumns as $key) {
            unset($this->attributes[$key]);
        }
    }

    public function commitSupertype()
    {
        /** @var \Illuminate\Database\Eloquent\Model */
        $supertype = $this->supertype();
        $supertype->forceFill($this->supertypeAttributes);
        $supertype->save();

        $this->setSupertypeKey($supertype->getKey());
    }

    public function getSupertypeKeyName(): string
    {
        return $this->supertypeKey;
    }

    public function setSupertypeKeyName(string $key): self
    {
        $this->supertypeKey = $key;

        return $this;
    }

    public function getQualifiedSupertypeKeyName()
    {
        return $this->qualifyColumn($this->getSupertypeKeyName());
    }

    public function getSupertypeKey()
    {
        return $this->getAttribute($this->getSupertypeKeyName());
    }

    public function setSupertypeKey(mixed $value)
    {
        return $this->setAttribute($this->getSupertypeKeyName(), $value);
    }

    public function scopeIncludeSuperColumns(Builder $builder): Builder
    {
        return $builder->addSelect($this->supertypeModel()->qualifiedDescendibleColumns());
    }

    public function scopeWithSuperColumns(Builder $builder): Builder
    {
        return $this->scopeIncludeSuperColumns($builder);
    }

    public function scopeSearchIncludeSupertype(Builder $builder, $term, $columns = []): Builder
    {
        $supertypeColumns = [];
        $inheritedColumns = $this->supertype()->descendibleColumns();
        foreach ($columns as $key => $qualifiedColumn) {
            [$table, $column] = explode('.', $qualifiedColumn, 2);

            if (in_array($column, $inheritedColumns)) {
                unset($columns[$key]);
                array_push($supertypeColumns, $this->supertypeModel()->qualifyColumn($column));
            }
        }

        return $builder->where(function($q) use ($term, $columns, $supertypeColumns) {
            $q->searchWhere($term, $columns)
                ->orWhereHas($this->supertype, function($q) use ($term, $supertypeColumns) {
                    $q->searchWhere($term, $supertypeColumns);
                });
        });
    }
}
