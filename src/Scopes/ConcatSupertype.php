<?php

namespace Empu\EloquentSubtype\Scopes;

use Empu\EloquentSubtype\Contracts\InteractWithSupertype;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ConcatSupertype implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model): void
    {
        if ($model instanceof InteractWithSupertype) {
            /** @var Model */
            $super = $model->supertype();
            $builder->join(
                $super->getTable(),
                $super->getQualifiedKeyName(), '=', $model->getQualifiedSupertypeKeyName()
            );//->includeSuperColumns();
        }
    }
}