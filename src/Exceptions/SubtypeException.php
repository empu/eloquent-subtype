<?php

namespace Empu\EloquentSubtype\Exceptions;

use Exception;

class SubtypeException extends Exception {}
