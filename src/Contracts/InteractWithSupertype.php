<?php

namespace Empu\EloquentSubtype\Contracts;

interface InteractWithSupertype
{
    public function supertype(): InheritableEntity;
}
