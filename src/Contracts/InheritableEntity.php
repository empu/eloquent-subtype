<?php

namespace Empu\EloquentSubtype\Contracts;

interface InheritableEntity
{
    public function descendibleColumns(): array;
}
